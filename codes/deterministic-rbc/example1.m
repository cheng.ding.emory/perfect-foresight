% Calibrate the deep parameters.
alpha = 1.0/3.0;
beta = .99;
delta = .02;

% Copy the deep parameters in a column vector.
parameters = [alpha; beta; delta];

% Set the number of periods
T = 200;

% Simulate paths with initial condition k0=.5*kstar
[k, c] = modelsimulation(T, .5*kstar, parameters);