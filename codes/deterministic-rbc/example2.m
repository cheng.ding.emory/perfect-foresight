% Calibrate the deep parameters.
alpha = 1.0/3.0;
beta = .99;
delta = .02;

% Copy the deep parameters in a column vector.
parameters = [alpha; beta; delta];

% Set the number of periods
T = 200;

% Compute the steady state of the model.
y = modelsteadystate(parameters);
kstar = y(1);
cstar = y(2);

% Create paths for the endogenous variables (this is a very rought initial guess).
Y = [cstar; repmat(y, T-1, 1); kstar];

maxiter = 100;
warning('off','all')
verbose = 0;

% Homotopy
KINIT = [.2:-.01:.05]*kstar;
steps = length(KINIT);

for i = 1:steps
    kinit = KINIT(i);
    noconvergence = 1; iter = 1;
    while noconvergence
        % Evaluate residuals and jacobian
        [F, J] = modelevaluation(Y, parameters, kinit, cstar);
        % Newton iteration
        Y = Y - J\F;
        % Test for convergence
        if max(abs(F))<1e-5
            noconvergence = 0;
            if verbose
                disp('=> Convergence achieved!')
            end
            k = [kinit; Y(1+(1:2:2*T))];
            c = [Y(1:2:2*T); cstar];
        else
            if verbose
                disp(['Iteration number ' int2str(iter) ', max. abs. residual is ' num2str(max(abs(F)))])
            end
            iter = iter+1;
        end
        % Stop if the number of iterations gets to large
        if iter>=maxiter
            if verbose
                disp(['Did not converge after ' int2str(maxit) ' iterations!'])
            end
            k = []; c = [];
            return
        end
        % Stop if the residual gets too large or NaN
        if isnan(max(abs(F))) || max(abs(F))>1e100
            if verbose
                disp('Newton algorithm diverged!')
            end
            k = []; c = [];
            return
        end
    end
    if isempty(k)
        disp(['I am not able to solve the model with homotopy (failed with kinit=' double2str(kinit) ')'])
        return
    end
end

warning('on','all')