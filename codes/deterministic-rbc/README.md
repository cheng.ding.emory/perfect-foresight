Simulation of a basic deterministic RBC model
=============================================

This code shows how to simulate a transition in a basic Business Cycle model (log utility function, fixed labour supply, Cobb-Douglas technology, fixed labour efficiency) with perfect foresight. The matlab's script [example1.m](http://git.ithaca.fr/perfect-foresight-models/src/master/codes/deterministic-rbc/example1.m) simulates paths for consumption and the physical capital stock when the initial level of the physical capital stock is half its long run level.
