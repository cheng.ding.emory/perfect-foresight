// Stochastic extended path simulation
@#include "rbc.mod"

steady_state_model;
  LoggedProductivity = LoggedProductivityInnovation/(1-rho);
  Capital = (exp(LoggedProductivity)*alpha/(1/beta-1+delta))^(1/(1-alpha));
  Consumption = exp(LoggedProductivity)*Capital^alpha-delta*Capital;
end;

set_time(1Q1);

steady;

shocks;
    var LoggedProductivityInnovation = .01^2;
end;

options_.ep.stochastic.quadrature.nodes = 3;
extended_path(periods=1000,order=1);

plot(Simulated_time_series.Capital,Simulated_time_series.Consumption,'ok')
