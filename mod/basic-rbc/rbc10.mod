// Permanent shock on productivity -> Simulation of the transition induced by a sequence of non expected shocks.
@#include "rbc.mod"

steady_state_model;
  LoggedProductivity = LoggedProductivityInnovation/(1-rho);
  Capital = (exp(LoggedProductivity)*alpha/(1/beta-1+delta))^(1/(1-alpha));
  Consumption = exp(LoggedProductivity)*Capital^alpha-delta*Capital;
end;

set_time(1Q1);

initval; LoggedProductivityInnovation = 0; end; steady;

endval; LoggedProductivityInnovation = 0; end; steady;

sequence_of_shocks = [.1; .2; -.2; -.2^2; -.5]; // [.1; .2; .2; .2^2; .2^4];

shocks;
    var LoggedProductivityInnovation; periods 1; values (sequence_of_shocks(1));
end;

yy = oo_.steady_state;
// First period
perfect_foresight_setup(periods=200);
perfect_foresight_solver;
yy = [yy, oo_.endo_simul(:,2)];

// Following periods
for i=2:length(sequence_of_shocks)
  oo_.exo_simul(2) = sequence_of_shocks(i);
  oo_.endo_simul(:,1) = yy(:,end);
  perfect_foresight_solver;
  yy = [yy, oo_.endo_simul(:,2)];
end;

yy = [yy, oo_.endo_simul(:,3:end)]; // Complete the paths with the last simulation.
Simulated_time_series = dseries(transpose(yy),1Q1,M_.endo_names);

plot(Simulated_time_series.Capital(1Q1:25Q4));
save('rbc10_simulated_data_surprise','Simulated_time_series');
